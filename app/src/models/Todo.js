const mongoose = require('mongoose');

const TodoSchema = new mongoose.Schema({
    tittle: {
        type: String,
        max: 256,
        min: 6,
        required: true
    },
    completed: {
        type: Boolean,
        required: true,
        default: false
    }
});


module.exports = mongoose.model('Todo', TodoSchema);
