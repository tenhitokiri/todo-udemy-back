const router = require('express').Router();
const TodoModel = require('../models/Todo');

router.get('/', (_, res) => {
    TodoModel.find((err, result) => {
        if (err) throw new Error(err)
        res.json(result)
    })
})
router.get('/:id', (req, res) => {
    TodoModel.findOne({_id: req.params.id}, (err, result) => {
        if (err) throw new Error(err)
        res.json(result)
    })
})

router.post('/', (req, res) => {
    console.log(req.body)
    TodoModel.create(req.body, (err, result) => {
        if (err) throw new Error(err)
        console.log(result)
        res.json(result)
    })
})

router.put('/:id', (req, res) => {
    TodoModel.findOneAndUpdate({_id: req.params.id}, req.body, {new : true}, (err, result) => {
        if (err) throw new Error(err)
        console.log(result)
        res.json(result)        
    })
})

router.delete('/:id', (req, res) => {
    TodoModel.findOneAndDelete({_id: req.params.id}, (err, result) => {
        if (err) throw new Error(err)
        console.log(result)
        res.json(result)        
    })
})

module.exports = router