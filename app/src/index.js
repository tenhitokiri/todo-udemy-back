const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const morgan = require('morgan');
const todoRouter = require('./routes/todoRoutes');

//activar modo desarrollo
const modo = process.env.MODO;
if (!modo) {
	const dotenv = require('dotenv').config();
	console.log('modo dev');
}

const app = express();
app.use(express.json())
app.use(cors())

//Configuración de la aplicación
const db = process.env.DB_CONNECT;

//Conectar a Mongodb
mongoose.connect(db, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
	useFindAndModify:false
})
.then(() => console.log('MongoDB Conectada ... xD'))
.catch(err => console.error("error: " +err));

//Middleware

//RUTAS
app.get('/', (req, res) => {
	const message = `La api esta en /api.`;
	res.json({
		message
	});
});

app.use("/todos", todoRouter)

//404 handle
app.use((req, res, next) => {
	res.status(404).json({
		err: 'Error, Ruta no encontrada'
	});
	next();
});

module.exports = app;
